﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Tools.Delegate;

[AddComponentMenu("Logic/Context")]
public class Context : MonoBehaviour
{
    
    public Delegate onClickRestart;
    public Delegate onClickNext;
    public event Delegate<RaycastHitObject> onBeginTouch;
    public event Delegate<RaycastHitObject> onDragTouch;
    public event Delegate<RaycastHitObject> onEndTouch;

    [Header("Scene")]
    public string _nextScene = "none";
    [Header("User Interface")]
    public StarPreView _starPreView;
    public StarPreView _starPreViewMenu;
    public RectTransform _menu;
    public Button _btnNext;
    public Text _status;
    public Text _txt_btn;
    [Space]
    [Header("Input")]
    public InputSystemComponent input;

    [Space]
    [Header("Ways")]
    public List<Way> ways = new List<Way>();

    private void Awake()
    {
        int countStarAll = 0;

        for (int i = 0; i < ways.Count; i++)
        {
            ways[i].Car.Id = i;
            ways[i].Car.Init();

            ways[i].Line.Id = i;
            ways[i].BeginPosition.Id = i;
            ways[i].EndPosition.Id = i;

            int length = ways[i].Stars.Length;
            for (int j = 0; j < length; j++)
            {
                ++countStarAll;
                ways[i].Stars[j].Id = i;
                ways[i].Stars[j].OnTake += OnTakeStar;
            }
        }

        _starPreView.CountAll = countStarAll;
        _starPreViewMenu.CountAll = countStarAll;
    }

    private void Start()
    {
        state = new StateDrawWays(this);
    }

    IState state = null;
    private void Update()
    {
        state = state?.Execute();
    }

    public void OnClickNext()
    {
        onClickNext?.Invoke();
    }

    public void OnClickRestart()
    {
        onClickRestart?.Invoke();
    }

    public void OnBeginTouch (RaycastHitObject hitObject)
    {
        onBeginTouch?.Invoke(hitObject);
    }

    public void OnDragTouch(RaycastHitObject hitObject)
    {
        onDragTouch?.Invoke(hitObject);
    }

    public void OnEndTouch(RaycastHitObject hitObject)
    {
        onEndTouch?.Invoke(hitObject);
    }

    public void OnTakeStar(int id)
    {
        Debug.Log("On Take Star Car id: " + id);
        _starPreView.ActiveStar();
        _starPreViewMenu.ActiveStar();
        ways[id].TakedStars += 1;
    }
}
