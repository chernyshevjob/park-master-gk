﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateRide : IState
{
    Context _context;
    List<Way> ways;
    int countWays;
    public StateRide(Context context)
    {
        _context = context;
        ways = context.ways;
        countWays = ways.Count;
        for (int i = 0; i < countWays; i++)
        {
            ways[i].Car.Points = context.ways[i].Line.Points;
            ways[i].Car.enabled = true;
            ways[i].Car.OnCollision += OnCollisionCars;
            ways[i].EndPosition.OnFinishEnter += OnFinishEnterCars;
            ways[i].EndPosition.OnFinishExit += OnFinishExitCars;
        }

        state = this;
    }

    IState state;

    public IState Execute()
    {
        return state;
    }

    int countFinished = 0;
    public void OnFinishEnterCars(int carId, int endId)
    {
        Debug.Log("Finish Enter car id: " + carId + ", end id: " + endId);
        if (carId == endId)
            ++countFinished;
        if (countFinished == countWays)
        {
            // All cars finish
            state = new StateWin(_context);
        }
    }

    public void OnFinishExitCars(int carId, int endId)
    {
        Debug.Log("Finish Exit id: " + carId + ", end id: " + endId);
        if (carId == endId)
            --countFinished;
    }

    bool isCollision = false;
    public void OnCollisionCars()
    {
        Debug.Log("Collision Cars");
        if (!isCollision)
        {
            isCollision = true;
            state = new StateLose(_context);
        }
    }
}
