﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateDrawWays : IState
{
    Context _context;
    InputSystemComponent _input;
    //Light _startLight;

    List<Vector3[]> _paths;
    public List<Vector3[]> Paths { get { return _paths; } }

    List<Way> _ways;
    int _count;

    // Ключи построения пути, активны если пути построены
    bool[] _isKeys;

    public StateDrawWays(Context context)
    {
        _state = this;
        _context = context;
        _input = context.input;
        _ways = context.ways;
        _count = _ways.Count;

        _isKeys = new bool[_count];
        for (int i = 0; i < _count; i++)
            _isKeys[i] = false;

        context.onBeginTouch += Context_onBeginTouch;
        context.onDragTouch += Context_onDragTouch;
        context.onEndTouch += Context_onEndTouch;
    }

    LineComponent line = null;
    Vector3 point;
    int id = -1;

    private void Context_onBeginTouch(RaycastHitObject arg)
    {
        if (arg.isHit)
        {
            string tag = arg.hit.transform.tag;
            Debug.Log("onBeginTouch: " + tag);

            if (tag == "BeginPoint")
                id = arg.hit.transform.GetComponent<BeginComponent>().Id;

            if (id != -1)
            {
                point = arg.point;
                line = _context.ways[id].Line;
                line.Clear();
                line.AddPoint(arg.point);
                _isKeys[id] = true;
            }
        }
    }

    private void Context_onDragTouch(RaycastHitObject arg)
    {
        if (arg.isHit)
        {
            string tag = arg.hit.transform.tag;
            Debug.Log("onDragTouch: " + tag);

            if (id != -1)
            {
                if (tag == "Barrier")
                {
                    line.Clear();
                    _isKeys[id] = false;
                    id = -1;
                    return;
                }

                Vector3 pos = arg.point;
                pos.y = 0.02f;
                float distance = Vector3.Distance(pos, point);

                if (distance > 0.001f)
                {
                    line.AddPoint(pos);
                    point = pos;
                }

                if (distance > 0.68f)
                {
                    line.Clear();
                    _isKeys[id] = false;
                    id = -1;
                    return;
                }

                
            }
        }
    }

    private void Context_onEndTouch(RaycastHitObject arg)
    {
        if (arg.isHit)
        {
            string tag = arg.hit.transform.tag;
            Debug.Log("onEndTouch: " + tag);

            int t_id = -1;

            if (tag == "EndPoint")
                t_id = arg.hit.transform.GetComponent<EndComponent>().Id;

            if (id != t_id)
            {
                line.Clear();
                _isKeys[id] = false;
            }

            // TODO
            // Нужно проверить что все пути построенны и изменит состояние игрового мира
            // Возможно подойдет bool массив

            bool res = true;

            // Если хотя бы один ключ не активен, то вернем текущее состояние
            foreach (bool key in _isKeys)
                if (!key)
                    res = false;

            if (res)
            {
                _context.onBeginTouch -= Context_onBeginTouch;
                _context.onDragTouch -= Context_onDragTouch;
                _context.onEndTouch -= Context_onEndTouch;
                _state = new StateRide(_context);
            }
                

            id = -1;
        }
    }

    ~StateDrawWays()
    {
        
    }

    IState _state = null;

    public IState Execute()
    {
        return _state;
    }


}
