﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateWin : IState
{
    Context _context;
    public StateWin (Context context)
    {
        _context = context;

        for (int i = 0; i < context.ways.Count; i++)
        {
            context.ways[i].Car.enabled = false;
        }

        context._menu.gameObject.SetActive(true);
        context._starPreView.gameObject.SetActive(false);
        context._btnNext.gameObject.SetActive(true);

        context._status.text = "YOU WIN!";
        context._txt_btn.text = "NEXT";
        context.onClickRestart = () =>
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        };

        context.onClickNext = () =>
        {
            //Scene scene = SceneManager.GetSceneByBuildIndex(context._nextNumberScene);
            SceneManager.LoadScene(context._nextScene);
        };
    }

    public IState Execute()
    {
        return this;
    }
}
