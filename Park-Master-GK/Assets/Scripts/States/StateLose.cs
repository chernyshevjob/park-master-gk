﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateLose : IState
{
    Context _context;
    public StateLose (Context context)
    {
        _context = context;

        for (int i = 0; i < context.ways.Count; i++)
        {
            context.ways[i].Car.enabled = false;
        }

        context._menu.gameObject.SetActive(true);
        context._starPreView.gameObject.SetActive(false);
        context._btnNext.gameObject.SetActive(false);

        context._status.text = "YOU LOSE:(";
        context._txt_btn.text = "RESTART";
        context.onClickRestart = () =>
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        };
    }

    public IState Execute()
    {
        return this;
    }
}
