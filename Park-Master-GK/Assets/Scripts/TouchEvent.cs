﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[AddComponentMenu("Logic/Touch Event")]
public class TouchEvent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler , IPointerDownHandler
{
    public enum TouchStates { Null, Begin, End }

    [Header("Event Position")]
    public UnityEvent<Vector3> OnPointTouch;

    [Space]

    [Header("Event State")]
    public UnityEvent<TouchStates> OnStateTouch;

    Camera _camera;

    bool isTouch = false;

    PointerEventData data;

    private void Awake()
    {
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        Input.simulateMouseWithTouches = true;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isTouch = true;
        data = eventData;

        // передаем состояние
        OnStateTouch?.Invoke(TouchStates.Begin);
    }

    public void OnDrag(PointerEventData eventData)
    {
        data = eventData;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isTouch = false;
        data = eventData;

        // передаем состояние
        OnStateTouch?.Invoke(TouchStates.End);
    }

    Vector3 point(PointerEventData eventData)
    {
        RaycastHit hit;
        Ray ray = _camera.ScreenPointToRay(eventData.position);
        Physics.Raycast(ray, out hit);

        return hit.point;
    }

    private void Update()
    {
        if (isTouch)
        {
            // передаем позиции
            OnPointTouch?.Invoke(point(data));
        }

        if (Input.touchCount > 0)
        {
            RaycastHit hit;
            Ray ray = _camera.ScreenPointToRay(Input.GetTouch(0).position);
            Physics.Raycast(ray, out hit);
            Debug.Log(hit.point);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isTouch = false;
        data = eventData;
    }
}
