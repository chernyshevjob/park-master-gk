﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Delegate
{
    public delegate void Delegate();
    public delegate void Delegate<T>(T arg);
    public delegate void Delegate<T0, T1>(T0 arg0, T1 arg1);
    public delegate void Delegate<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2);
    public delegate void Delegate<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3);
    public delegate void Delegate<T0, T1, T2, T3, T4>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    public delegate void Delegate<T0, T1, T2, T3, T4, T5>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
    public delegate void Delegate<T0, T1, T2, T3, T4, T5, T6>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
}