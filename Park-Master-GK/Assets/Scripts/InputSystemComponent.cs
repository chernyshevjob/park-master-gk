﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public struct RaycastHitObject
{
    public bool isHit;
    public RaycastHit hit;
    public Vector3 point;
}

[AddComponentMenu("Logic/Input System")]
public class InputSystemComponent : MonoBehaviour , IBeginDragHandler , IDragHandler, IEndDragHandler , IPointerDownHandler , IPointerUpHandler
{
    [Header("Events")]
    public UnityEvent<RaycastHitObject> OnBeginTouch;
    public UnityEvent<RaycastHitObject> OnDragTouch;
    public UnityEvent<RaycastHitObject> OnEndTouch;

    [Space]
    [SerializeField] LayerMask _ignoreLayer;

    Camera _camera;

    public bool isTouch { get; private set; }
    public RaycastHit Point { get; private set; }
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("Grag BEGIN");
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Grag");
        OnDragTouch?.Invoke(point(eventData));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("Grag END");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.Log("Point DOWN");
        isTouch = true;
        OnBeginTouch?.Invoke(point(eventData));
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //Debug.Log("Point UP");
        isTouch = false;
        OnEndTouch?.Invoke(point(eventData));
    }

    private void Awake()
    {
        isTouch = false;
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    RaycastHitObject point(PointerEventData eventData)
    {
        RaycastHitObject hitObject;
        hitObject.isHit = false;
        RaycastHit hit;
        Ray ray = _camera.ScreenPointToRay(eventData.position);
        if (Physics.Raycast(ray, out hit, 100, _ignoreLayer))
        {
            hitObject.isHit = true;
        }
        hitObject.hit = hit;
        hitObject.point = hit.point;

        return hitObject;
    }
}
