﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Star View Data", menuName = "Data/Star View Data", order = 51)]
public class StarViewData : ScriptableObject
{
    [SerializeField] Color _colorEnable = Color.white;
    public Color ColorEnable { get { return _colorEnable; } }

    [SerializeField] Color _colorDisable = Color.white;
    public Color ColorDisable { get { return _colorDisable; } }
}
