﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Logic/Star Preview Component")]
public class StarPreView : MonoBehaviour
{
    [SerializeField] StarViewData _viewData;

    [SerializeField] Image[] _images;

    int _countStarsActive = 0;
    int _countStarsAll = 0;

    public int CountAll
    {
        get
        {
            return _countStarsAll;
        }

        set
        {
            _countStarsAll = value;
            Debug.Log("Count Stars: " + value);
            for (int i = 0; i < value; i++) // необходимые звезды включаем
            {
                _images[i].gameObject.SetActive(true);
                _images[i].color = _viewData.ColorDisable;
            }

            int length = _images.Length;
            for (int i = value; i < length; i++) // лишние звезды выключаем
            {
                _images[i].gameObject.SetActive(false);
            }
        }
    }

    // Активируем звезду
    public int ActiveStar()
    {
        _images[_countStarsActive].color = _viewData.ColorEnable;
        ++_countStarsActive;
        return _countStarsActive;
    }

    public int ActiveStar(int count)
    {
        for (int i = 0; i < count; i++)
        {
            _images[_countStarsActive].color = _viewData.ColorEnable;
            ++_countStarsActive;
        }
        
        return _countStarsActive;
    }
}
