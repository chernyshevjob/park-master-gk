﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tools.Delegate;

[AddComponentMenu("Logic/Star Component")]
public class StarComponent : MonoBehaviour
{
    public event Delegate<int> OnTake;

    public int Id { get; set; }


    [Header("Data")]
    [SerializeField] WayData _wayData;

    MeshRenderer _mr;
    private void Start()
    {
        _mr = GetComponent<MeshRenderer>();
        _mr.material.color = _wayData.Color;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("On Trigger Enter Star id: " + Id);
        GameObject o = other.gameObject;
        if (o.tag == "Car")
        {
            CarComponent car = o.GetComponent<CarComponent>();
            Debug.Log("Car Id: " + car.Id + ", Star Id: " + Id);
            if (car.Id == Id)
            {
                OnTake?.Invoke(Id);
                gameObject.SetActive(false);
            }
        }
    }
}
