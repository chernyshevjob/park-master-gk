﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tools.Delegate;

[AddComponentMenu("Logic/Car Component")]
public class CarComponent : MonoBehaviour
{
    int id;
    public int Id {
        get { return id; }
        set {
            Debug.Log("Car Id: " + value);
            id = value; 
        } 
    }

    public Delegate OnCollision;

    [SerializeField] float _speed = 1;
    [SerializeField] float _turn = 1;

    List<Vector3> _points = new List<Vector3>();
    public List<Vector3> Points
    { 
        get 
        { 
            return _points; 
        }
        set 
        { 
            _points = value; 
            count = value.Count; 
        }
    }

    [Header("Data")]
    public WayData _wayData;

    MeshRenderer _mr;

    public void Init ()
    {
        _mr = transform.GetComponentInChildren<MeshRenderer>();
        _mr.material.color = _wayData.Color;
    }

    void Awake()
    {
        count = _points.Count;
    }

    int index = 0;
    int count = 0;
    void Update()
    {
        Vector3 carPosition = transform.position;
        Vector3 pointPosition = _points[index];
        float distance = Vector3.Distance(carPosition, pointPosition);
        if (distance < 0.2)
        {
            ++index;
            Debug.Log("Count: " + count + ", Index: " + index);
            if (index > count - 1)
            {
                Debug.Log("Car Component Disable!");
                this.enabled = false;
            }
        }

        Vector3 direction = (pointPosition - carPosition).normalized;
        direction.y = 0;
        transform.Translate(direction * _speed * Time.deltaTime, Space.World);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), _turn * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        string tag = collision.gameObject.tag;
        if (tag == "Car")
        {
            Debug.Log("Car Collision");
            OnCollision?.Invoke();
        }    
    }
}
