﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools.Delegate;

[AddComponentMenu("Logic/End Component")]
public class EndComponent : MonoBehaviour
{
    public Delegate<int,int> OnFinishEnter;
    public Delegate<int,int> OnFinishExit;

    [Header("Data")]
    [SerializeField] WayData _wayData;

    MeshRenderer _mr;

    public int Id { get; set; }
    void Start()
    {
        _mr = GetComponent<MeshRenderer>();
        _mr.material = new Material(_wayData.MaterialParkingSpace);
        _mr.material.color = _wayData.Color;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("End TriggerEnter Id: " + Id);
        if (other.gameObject.tag == "Car")
        {
            int carId = other.gameObject.GetComponent<CarComponent>().Id;
            StartCoroutine(Wait(carId));
            //OnFinishEnter?.Invoke(carId, Id);
        }
    }

    IEnumerator Wait (int carId)
    {
        yield return new WaitForSeconds(0.3f);
        OnFinishEnter?.Invoke(carId, Id);
        yield return null;
    }

    private void OnTriggerStay(Collider other)
    {

    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("End TriggerExit Id: " + Id);
        if (other.gameObject.tag == "Car")
        {
            int carId = other.gameObject.GetComponent<CarComponent>().Id;
            OnFinishExit?.Invoke(carId,Id);
        }
    }
}
