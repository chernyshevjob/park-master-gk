﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Logic/Begin Component")]
public class BeginComponent : MonoBehaviour
{
    public int Id { get; set; }

    [Header("Data")]
    [SerializeField] WayData _wayData;

    MeshRenderer _mr;

    void Awake()
    {
        _mr = GetComponent<MeshRenderer>();
        _mr.material = new Material(_wayData.MaterialStartPosition);
        _mr.material.color = _wayData.Color;
    }
}
