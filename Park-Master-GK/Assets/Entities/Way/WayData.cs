﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Way Data", menuName = "Data/Way Data", order = 51)]
public class WayData : ScriptableObject
{
    [SerializeField] Material _materialStartPosition;
    public Material MaterialStartPosition { get { return _materialStartPosition; } }

    [SerializeField] Material _materialParkingSpace;
    public Material MaterialParkingSpace { get { return _materialParkingSpace; } }

    [SerializeField] float _width = 1f;
    public float Width { get { return _width; } }

    [SerializeField] Color _color = Color.white;

    public Color Color { get { return _color; } }

    [SerializeField] Gradient _gradient;

    public Gradient Gradient { get { return _gradient; } }
}
