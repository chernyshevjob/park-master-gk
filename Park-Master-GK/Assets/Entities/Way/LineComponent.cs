﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Logic/Line Component")]
public class LineComponent : MonoBehaviour
{
    public int Id { get; set; }

    LineRenderer _line;

    [Header("Data")]
    public WayData _wayData;

    //List<Vector3> _points;
    int count;
    public List<Vector3> Points { 
        get 
        {
            List<Vector3> _points = new List<Vector3>();
            int length = _line.positionCount;
            for (int i = 0; i < length; i++)
                _points.Add(_line.GetPosition(i));
            return _points;
        }

        set
        {
            count = value.Count;
            _line.positionCount = count;
            _line.SetPositions(value.ToArray());
        }
    }

    public void AddPoint (Vector3 position)
    {
        ++count;
        _line.positionCount = count;
        _line.SetPosition(count - 1, position);
    }

    void Start()
    {
        count = 0;
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.Euler(90, 0, 0);

        _line = GetComponent<LineRenderer>();
        _line.useWorldSpace = true;
        _line.alignment = LineAlignment.TransformZ;
        //_line.material = wayData.MaterialLineRenderer;
        //_line.colorGradient = _wayData.Gradient;
        _line.material.color = _wayData.Color;
        _line.startWidth = _wayData.Width;
        _line.endWidth = _wayData.Width;
    }

    public void Clear ()
    {
        count = 0;
        _line.positionCount = 0;
    }
}
