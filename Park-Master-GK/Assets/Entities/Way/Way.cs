﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Way
{
    public CarComponent Car;

    public LineComponent Line;

    public BeginComponent BeginPosition;
    public EndComponent EndPosition;

    public StarComponent[] Stars;

    int takedStars = 0;

    public int TakedStars
    {
        get
        {
            return takedStars;
        }

        set
        {
            takedStars = value;
        }
    }
}