﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Logic/Light/PingPong")]
public class LightPingPong : MonoBehaviour
{
    [Header("Intensity")]
    [SerializeField] float min = 0;
    [SerializeField] float max = 1;
    [Header("Speed")]
    [SerializeField] float speed = 1;
    //[SerializeField] [Range(-1, 1)] float delta;

    Light light;
    float delta;
    private void Awake()
    {
        light = GetComponent<Light>();
        delta = max - min;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        light.intensity = Mathf.PingPong(Time.time * speed, delta) + min;
    }
}
